Source: qt6-serialbus
Section: libs
Priority: optional
Maintainer: Debian Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
Uploaders: Patrick Franz <deltaone@debian.org>,
           Lu YaNing <dluyaning@gmail.com>
Build-Depends: cmake (>= 3.18~),
               debhelper-compat (= 13),
               libgl-dev,
               libqt6opengl6-dev (>= 6.2.1+dfsg~),
               libqt6serialport6-dev (>= 6.2.1~),
               libvulkan-dev [linux-any],
               linux-libc-dev [linux-any],
               ninja-build,
               pkg-config,
               pkg-kde-tools,
               qt6-base-dev (>= 6.2.1+dfsg~),
               qt6-base-private-dev (>= 6.2.1+dfsg~),
Standards-Version: 4.6.0
Homepage: https://www.qt.io/developers/
Vcs-Browser: https://salsa.debian.org/qt-kde-team/qt6/qt6-serialbus
Vcs-Git: https://salsa.debian.org/qt-kde-team/qt6/qt6-serialbus.git
Rules-Requires-Root: no

Package: libqt6serialbus6
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: Qt serialbus - serial bus access shared library
 Qt serialbus module provides Qt module for general purpose
 serial bus access. Support for CAN and potentially other
 serial buses.
 .
 This package contains the shared library for Qt Serialbus.

Package: libqt6serialbus6-bin
Architecture: linux-any
Section: utils
Multi-Arch: foreign
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: Qt Serialbus module serial bus access helper binaries
 Qt serialbus module provides Qt module for general purpose
 serial bus access. Support for CAN and potentially other
 serial buses.
 .
 This package contains Serialbus helper binaries.

Package: libqt6serialbus6-plugins
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: libqt6serialbus6 (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends},
Description: Qt serialbus - serial bus access plugins
 Qt serialbus module provides Qt module for general purpose
 serial bus access. Support for CAN and potentially other
 serial buses.
 .
 This package contains Qt Serialbus plugins.

Package: libqt6serialbus6-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: libqt6serialbus6 (= ${binary:Version}),
         libqt6serialbus6-plugins (= ${binary:Version}),
         qt6-base-dev,
         ${misc:Depends},
Description: Qt serialbus serial bus access development
 Qt serialbus module provides Qt module for general purpose
 serial bus access. Support for CAN and potentially other
 serial buses.
 .
 This package contains the header development files used for building Qt 6
 applications using qtserialbus.
